#version 330

uniform mat4 projection;
uniform mat4 camera;
uniform mat4 view;
uniform mat4 model;
uniform vec3 light;
uniform float shininess;

in vec3 fcolor;
in vec4 fposition;
in vec4 fnormal;

out vec4 color_out;

void main() {
  vec4 transformedlight = projection * view * camera * model * vec4(light, 1);
  
  vec4 toLight = transformedlight - fposition;
  vec4 toV = -normalize(fposition);
  vec4 normal = normalize(fnormal);
  vec4 h = normalize(toV + toLight);
  
  float specular = pow(max(0.0, dot(h, normal)), shininess);
  float diffuse = max(0.0, dot(normal, toLight));
  vec3 intensity = vec3(0.12,0.12,0.12) + (fcolor * diffuse) + (vec3(0.6,0.6,0.6) * specular);
  color_out = vec4(intensity.x,intensity.y,intensity.z, 1.0);
}