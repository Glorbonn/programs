# Tips

Use wasd and ctrl+space to scale x,y,z. Hopefully it's intuitive.

# Hotkeys

* v - toggle between 4 sided wall and single wall

* up arrow - add rows

* down arrow - delete rows

* right arrow - add columns

* left arrow -  delete columns

* z - switch to spacing bricks

* x - switch to sizing bricks (scaling)

* w/s - increase/ decrease y

* a/d decrease/ increase x (this is wysiwyg)

* ctrl/spacebar - increase/ decrease z

# Walls

Construct a wall or a small fort!

# Issues

* Resizing moves the grid!

* No zoom in/ out

* One light

# Screenshot

![Fort](example.PNG)



