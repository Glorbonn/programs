#version 330

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform mat4 camera;

in vec3 position;
in vec3 color;
in vec3 normal;

out vec3 fcolor;
out vec4 fposition;
out vec4 fnormal;

void main() {
  fposition = model*vec4(position,1);
  fnormal = transpose(inverse(model)) * vec4(normal, 0);
  fcolor = color;
  gl_Position = projection * view * camera * model * vec4(position, 1);
}
