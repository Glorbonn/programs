#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__

#include <QGLWidget>
#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QMouseEvent>
#include <glm/glm.hpp>
#include <vector>

#define GLM_FORCE_RADIANS

using glm::mat4;
using glm::vec2;
using glm::vec3;

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core { 
    Q_OBJECT

    public:
        GLWidget(QWidget *parent=0);
        ~GLWidget();

        GLuint loadShaders(const char* vertf, const char* fragf);
    protected:
        void initializeGL();
        void resizeGL(int w, int h);
        void paintGL();

        void mousePressEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);
		
		void keyPressEvent(QKeyEvent *event);

    private:
        void initializeCube();
        void renderCube(mat4 cubeToRender);
		void drawWall();
		void draw4Sided();
		
		GLuint numCubes;

        GLuint cubeProg;
        GLuint cubeVao;
        GLint cubeProjMatrixLoc;
        GLint cubeViewMatrixLoc;
        GLint cubeModelMatrixLoc;
		    
		GLuint positionBuffer;
		GLuint normalBuffer;
		GLuint colorBuffer;
		GLuint indexBuffer;
		GLuint grid_positionBuffer;
		
        void initializeGrid();
        void renderGrid();

        GLuint gridProg;
        GLuint gridVao;
		GLint lightLoc;
		GLint shinyLoc;
        GLint gridProjMatrixLoc;
        GLint gridViewMatrixLoc;
        GLint gridModelMatrixLoc;
		GLint cubeCameraMatrixLoc;
		GLint gridCameraMatrixLoc;

        mat4 projMatrix;
        mat4 viewMatrix;
        mat4 modelMatrix;
		mat4 cameraMatrix;

        int width;
        int height;
		
		int scalingDim;
		int rows;
		int columns;
		bool wallType;
		bool isSpacing;
		bool isScaling;
		bool isDims;
		float offset;
		
		vec3 spacing;
		vec3 color;		
		vec3 scaling;
		

        glm::vec3 lastVPt;
        glm::vec3 pointOnVirtualTrackball(const glm::vec2 &pt);
		
		std::vector<vec3> ptslist;
		std::vector<vec3> clrslist;
		std::vector<vec3> normslist;
		std::vector<GLuint> indicesList;
};

#endif
