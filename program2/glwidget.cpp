#include "glwidget.h"
#include <iostream>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <QTextStream>
#include <vector>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

using glm::inverse;
using glm::vec2;
using glm::vec3;
using glm::mat4;
using glm::perspective;
using glm::normalize;
using glm::length;
using glm::cross;
using glm::dot;
using glm::rotate;
using glm::value_ptr;
using glm::lookAt;
using glm::translate;
using glm::rotate;
using glm::scale;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent), numCubes(0) { 
	scaling = vec3(.8f,.5f,1.0f);
	color = vec3(1.0f,1.0f,1.0f);
	spacing = vec3(1.4f,1.7f,1.3f);
	
	offset = 1.0f;
	rows = 4;
	columns = 7;
	scalingDim = 0; //used for spacing and scaling dimensions
	isScaling = false;
	isDims = false;
	isSpacing = true;
	wallType = true; //normal wall or square
}

GLWidget::~GLWidget() {
}

void GLWidget::keyPressEvent(QKeyEvent *event) {
    switch(event->key()) {
        case Qt::Key_V:
			//std::cout << "Toggling wall option." << std::endl;
			wallType = !wallType;
			//update();
            break;
		case Qt::Key_Z:
			isSpacing = true;
			isScaling = false;
			isDims = false;
			break;
		case Qt::Key_X:
			isSpacing = false;
			isScaling = true;
			isDims = false;
			break;
		case Qt::Key_A:
			if (isSpacing) {
				spacing.x += .05f;
			}
			if (isScaling) {
				scaling.x += .05f;
			}
			break;
		case Qt::Key_Up:
			rows += 1;
			break;
		case Qt::Key_Down:
			rows -= 1;
			if (rows < 2 ) {
				rows = 2;
			}
			break;		
		case Qt::Key_Right:
			columns += 1;
			break;
		case Qt::Key_Left:
			columns -= 1;
			if (columns < 2) {
				columns = 2;
			}
			break;
		case Qt::Key_D:
			if (isSpacing) {
				spacing.x -= .05f;
			}
			if (isScaling) {
				scaling.x -= .05f;
			}
			break;
		case Qt::Key_W:
			if (isSpacing) {
				spacing.y += .05f;
			}
			if (isScaling) {
				scaling.y += .05f;
			}
			break;
		case Qt::Key_S:
			if (isSpacing) {
				spacing.y -= .05f;
			}
			if (isScaling) {
				scaling.y -= .05f;
			}
			break;
		case Qt::Key_Space:
			if (isSpacing) {
				spacing.z += .05f;
			}
			if (isScaling) {
				scaling.z += .05f;
			}
			break;
		case Qt::Key_Control:
			if (isSpacing) {
				spacing.z -= .05f;
			}
			if (isScaling) {
				scaling.z -= .05f;
			}
    }
	update();
}

void GLWidget::initializeGrid() {
    glGenVertexArrays(1, &gridVao);
    glBindVertexArray(gridVao);

    // Create a buffer on the GPU for position data
    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);

    vec3 pts[84];
    for(int i = -10; i <= 10; i++) {

        pts[2*(i+10)] = vec3(i, -.5f, 10);
        pts[2*(i+10)+1] = vec3(i, -.5f, -10);

        pts[2*(i+10)+42] = vec3(10,-.5f, i);
        pts[2*(i+10)+43] = vec3(-10,-.5f, i);
    }

    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/grid_vert.glsl", ":/grid_frag.glsl");
    glUseProgram(program);
    gridProg = program;

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information 
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    gridProjMatrixLoc = glGetUniformLocation(program, "projection");
    gridViewMatrixLoc = glGetUniformLocation(program, "view");
    gridModelMatrixLoc = glGetUniformLocation(program, "model");
    gridCameraMatrixLoc = glGetUniformLocation(program, "camera");
}


void GLWidget::initializeCube() {
    // Create a new Vertex Array Object on the GPU which
    // saves the attribute layout of our vertices.
    glGenVertexArrays(1, &cubeVao);
    glBindVertexArray(cubeVao);

    // Create a buffer on the GPU for position data
    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);
	
	GLuint normalBuffer;
	glGenBuffers(1, &normalBuffer);

    GLuint colorBuffer;
    glGenBuffers(1, &colorBuffer);

    GLuint indexBuffer;
    glGenBuffers(1, &indexBuffer);

    vec3 pts[] = {
        // top
        vec3(1,1,1),    // 0
        vec3(1,1,-1),   // 1
        vec3(-1,1,-1),  // 2
        vec3(-1,1,1),   // 3

        // bottom
        vec3(1,-1,1),   // 4
        vec3(-1,-1,1),  // 5
        vec3(-1,-1,-1), // 6
        vec3(1,-1,-1),  // 7

        // front
        vec3(1,1,1),    // 8
        vec3(-1,1,1),   // 9
        vec3(-1,-1,1),  // 10
        vec3(1,-1,1),   // 11

        // back
        vec3(-1,-1,-1), // 12
        vec3(-1,1,-1),  // 13
        vec3(1,1,-1),   // 14
        vec3(1,-1,-1),  // 15

        // right
        vec3(1,-1,1),   // 16
        vec3(1,-1,-1),  // 17
        vec3(1,1,-1),   // 18
        vec3(1,1,1),    // 19

        // left
        vec3(-1,-1,1),  // 20
        vec3(-1,1,1),   // 21
        vec3(-1,1,-1),  // 22
        vec3(-1,-1,-1)  // 23

    };
	
	vec3 norms[] = {
		// top
        vec3(0,1,0),    // 0
        vec3(0,1,0),    // 1
        vec3(0,1,0),    // 2
        vec3(0,1,0),    // 3

        // bottom
        vec3(0,-1,0),   // 4
        vec3(0,-1,0),   // 5
        vec3(0,-1,0),   // 6
        vec3(0,-1,0),   // 7

        // front
        vec3(0,0,1),    // 8
        vec3(0,0,1),    // 9
        vec3(0,0,1),    // 10
        vec3(0,0,1),    // 11

        // back
        vec3(0,0,-1),   // 12
        vec3(0,0,-1),   // 13
        vec3(0,0,-1),   // 14
        vec3(0,0,-1),   // 15

        // right
        vec3(1,0,0),  	// 16
        vec3(1,0,0),    // 17
        vec3(1,0,0),  	// 18
        vec3(1,0,0),    // 19

        // left
        vec3(-1,0,0),   // 20
        vec3(-1,0,0),   // 21
        vec3(-1,0,0),   // 22
        vec3(-1,0,0)    // 23
		
	
	};

    for(int i = 0; i < 24; i++) {
        pts[i] *= .5;
    }

    vec3 colors[] = {
        // top
        vec3(0,1,0),    
        vec3(0,1,0),    
        vec3(0,1,0),    
        vec3(0,1,0),    

        // bottom
        vec3(0,.5f,0),  
        vec3(0,.5f,0),  
        vec3(0,.5f,0),  
        vec3(0,.5f,0),  

        // front
        vec3(0,0,1),    
        vec3(0,0,1),    
        vec3(0,0,1),    
        vec3(0,0,1),    

        // back
        vec3(0,0,.5f),  
        vec3(0,0,.5f),  
        vec3(0,0,.5f),  
        vec3(0,0,.5f),

        // right
        vec3(1,0,0),    
        vec3(1,0,0),    
        vec3(1,0,0),    
        vec3(1,0,0),    


        // left
        vec3(.5f,0,0),  
        vec3(.5f,0,0),  
        vec3(.5f,0,0),  
        vec3(.5f,0,0)  
    };

    GLuint restart = 0xFFFFFFFF;
    GLuint indices[] = {
        0,1,2,3, restart,
        4,5,6,7, restart,
        8,9,10,11, restart,
        12,13,14,15, restart,
        16,17,18,19, restart,
        20,21,22,23
    };

    // Upload the position data to the GPU, storing
    // it in the buffer we just allocated.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);
	
	glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(norms), norms, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/vert.glsl", ":/frag.glsl");
    glUseProgram(program);
    cubeProg = program;

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information 
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    GLint colorIndex = glGetAttribLocation(program, "color");
    glEnableVertexAttribArray(colorIndex);
    glVertexAttribPointer(colorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);
	
	//added binding info for normal
	glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    GLint normalIndex = glGetAttribLocation(program, "normal");
    glEnableVertexAttribArray(normalIndex);
    glVertexAttribPointer(normalIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);
	
	lightLoc = glGetUniformLocation(program, "light");
	glUniform3f(lightLoc,10.0f,0.0f,0.0f);
	
	shinyLoc = glGetUniformLocation(program, "shininess");
	glUniform1f(shinyLoc, 12.0f);

    cubeProjMatrixLoc = glGetUniformLocation(program, "projection");
    cubeViewMatrixLoc = glGetUniformLocation(program, "view");
    cubeModelMatrixLoc = glGetUniformLocation(program, "model");
	cubeCameraMatrixLoc = glGetUniformLocation(program, "camera");
	
	//modelMatrix = modelLoc;
	//glUniformMatrix4fv(cubeModelMatrixLoc, 1, false, value_ptr(modelMatrix));
	
	//renderCube();
}


void GLWidget::initializeGL() {
    initializeOpenGLFunctions();

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glPointSize(4.0f);

    glEnable(GL_DEPTH_TEST);
    GLuint restart = 0xFFFFFFFF;
    glPrimitiveRestartIndex(restart);
    glEnable(GL_PRIMITIVE_RESTART);
	
	/*mat4 model = translate(mat4(1.0f), vec3(1,0,0));
    initializeCube(model);
	model = translate(mat4(1.0f), vec3(2,0,0));*/
	
	initializeCube();
    initializeGrid();
	std::cout << "Finished initialization..." << std::endl;
}

void GLWidget::resizeGL(int w, int h) {
    width = w;
    height = h;

    float aspect = (float)w/h;

    projMatrix = perspective(45.0f, aspect, 1.0f, 100.0f);
    viewMatrix = lookAt(vec3(0,0,-10),vec3(0,0,0),vec3(0,1,0));
    //modelMatrix = mat4(1.0f);

    glUseProgram(cubeProg);
    glUniformMatrix4fv(cubeProjMatrixLoc, 1, false, value_ptr(projMatrix));
    glUniformMatrix4fv(cubeViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(cubeModelMatrixLoc, 1, false, value_ptr(modelMatrix));
	glUniformMatrix4fv(cubeCameraMatrixLoc, 1, false, value_ptr(cameraMatrix));

    glUseProgram(gridProg);
    glUniformMatrix4fv(gridProjMatrixLoc, 1, false, value_ptr(projMatrix));
    glUniformMatrix4fv(gridViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(gridModelMatrixLoc, 1, false, value_ptr(modelMatrix));
	glUniformMatrix4fv(gridCameraMatrixLoc, 1, false, value_ptr(cameraMatrix));
}

void GLWidget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    renderGrid();
	if (wallType) {
		//std::cout << "Drawing 1 sided wall." << std::endl;
		drawWall();
	} else {
		//std::cout << "Drawing 4 sided wall." << std::endl;
		draw4Sided();
	}
	//mat4 cube1(1.0f);
	//cube1 = 
    //renderCube(translate(mat4(1.0f), vec3(2,0,0)));
	//renderCube(translate(mat4(1.0f), vec3(0,0,0)));
	//mat4 cube2(1.0f);
	//cube2 = translate(cube2, vec3(1,0,0));
	//renderCube(cube2);
}


//Draw a single wall! Positions are changed using translate and iterating using rows and columns.
void GLWidget::drawWall() {
	vec3 iterateSpacing(spacing.x, spacing.y, spacing.z); //unnecessary

	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < columns; j++) {

			renderCube(translate(mat4(1.0f), vec3(iterateSpacing.x*j,iterateSpacing.y*i,iterateSpacing.z)));
			//iterateSpacing.z += iterateSpacing.z;
			//spacing.z += spacing.z;
		}
		//std::cout << iterateSpacing.x << " " << iterateSpacing.y << " " << iterateSpacing.z << std::endl;
		//iterateSpacing.y += iterateSpacing.y;
	}

	//spacing.z 
}

void GLWidget::draw4Sided() {
	vec3 iterateSpacing(spacing.x, spacing.y, spacing.z);

		//draw front wall
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {

				renderCube(translate(mat4(1.0f), vec3(iterateSpacing.x*j,iterateSpacing.y*i,iterateSpacing.z)));
				//iterateSpacing.z += iterateSpacing.z;
				//spacing.z += spacing.z;
			}
			//std::cout << iterateSpacing.x << " " << iterateSpacing.y << " " << iterateSpacing.z << std::endl;
			//iterateSpacing.y += iterateSpacing.y;
		}
		
		//draw left and right walls
		for (int i = 0; i < rows; i++) { 
			for (int j = 0; j < columns; (j+=columns-1)) { //skip all but the first and last walls!
				for (int k = 1; k < columns; k++ ) { //make cubes in the right and left walls
				renderCube(translate(mat4(1.0f), vec3(iterateSpacing.x*j,iterateSpacing.y*i,iterateSpacing.z*k)));
				//iterateSpacing.z += iterateSpacing.z;
				//spacing.z += spacing.z;
				}
			}
			//std::cout << iterateSpacing.x << " " << iterateSpacing.y << " " << iterateSpacing.z << std::endl;
			//iterateSpacing.y += iterateSpacing.y;
		}
		
		//draw back wall
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {

				renderCube(translate(mat4(1.0f), vec3(iterateSpacing.x*j,iterateSpacing.y*i,iterateSpacing.z*columns)));
				//iterateSpacing.z += iterateSpacing.z;
				//spacing.z += spacing.z;
			}
			//std::cout << iterateSpacing.x << " " << iterateSpacing.y << " " << iterateSpacing.z << std::endl;
			//iterateSpacing.y += iterateSpacing.y;
		}
	


	//spacing.z 
}

//takes a mat4 for the model matrix of the cube, and renders it
void GLWidget::renderCube(mat4 cubeToRender) {
    glUseProgram(cubeProg);
    glBindVertexArray(cubeVao);
	
	cubeToRender = scale(cubeToRender, scaling);
	modelMatrix = cubeToRender;

	glUniformMatrix4fv(cubeModelMatrixLoc, 1, false, value_ptr(modelMatrix));
    //glDrawElementsInstanced(GL_TRIANGLE_FAN, indicesList.size()*sizeof(indicesList[0]), GL_UNSIGNED_INT, 0, numCubes);
	//glDrawElementsInstanced(GL_TRIANGLE_FAN, 29, GL_UNSIGNED_INT, indicesList.size()*sizeof(indicesList[0]) - 29, numCubes);
	glDrawElements(GL_TRIANGLE_FAN, 29, GL_UNSIGNED_INT, 0);
}

void GLWidget::renderGrid() {
    glUseProgram(gridProg);
    glBindVertexArray(gridVao);
    glDrawArrays(GL_LINES, 0, 84);
	
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
    GLuint program = glCreateProgram();

    // read vertex shader from Qt resource file
    QFile vertFile(vertf);
    vertFile.open(QFile::ReadOnly | QFile::Text);
    QString vertString;
    QTextStream vertStream(&vertFile);
    vertString.append(vertStream.readAll());
    std::string vertSTLString = vertString.toStdString();

    const GLchar* vertSource = vertSTLString.c_str();

    GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertShader, 1, &vertSource, NULL);
    glCompileShader(vertShader);
    {
        GLint compiled;
        glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( vertShader, len, &len, log );
            std::cout << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, vertShader);

    // read fragment shader from Qt resource file
    QFile fragFile(fragf);
    fragFile.open(QFile::ReadOnly | QFile::Text);
    QString fragString;
    QTextStream fragStream(&fragFile);
    fragString.append(fragStream.readAll());
    std::string fragSTLString = fragString.toStdString();

    const GLchar* fragSource = fragSTLString.c_str();

    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, &fragSource, NULL);
    glCompileShader(fragShader);
    {
        GLint compiled;
        glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( fragShader, len, &len, log );
            std::cerr << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, fragShader);

    glLinkProgram(program);

    return program;
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
    vec2 pt(event->x(), event->y());
    lastVPt = normalize(pointOnVirtualTrackball(pt));
}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
    vec2 pt(event->x(), event->y());
    vec3 vPt = normalize(pointOnVirtualTrackball(pt));

    vec3 axis = cross(lastVPt, vPt);
//    vec3 axis = cross(vPt, lastVPt);
    if(length(axis) >= .00001) {
        axis = normalize(axis);
        float angle = acos(dot(vPt,lastVPt));
        mat4 r = rotate(mat4(1.0f), angle, axis);

        cameraMatrix = cameraMatrix*r;

        glUseProgram(cubeProg);
        glUniformMatrix4fv(cubeCameraMatrixLoc, 1, false, value_ptr(cameraMatrix));

        glUseProgram(gridProg);
        glUniformMatrix4fv(gridCameraMatrixLoc, 1, false, value_ptr(cameraMatrix));
    }
    lastVPt = vPt;
    update();
}

vec3 GLWidget::pointOnVirtualTrackball(const vec2 &pt) {
    float r = .5f;
    float rr = r*r;
    vec3 p;
    p.x = -1 + pt.x*(2.0f/width);
    p.y = -(float)height/width*(1-pt.y*(2.0f/height));

    float xx = p.x*p.x;
    float yy = p.y*p.y;

    if(xx+yy <= rr*.5) {
        p.z = sqrt(rr-(xx+yy));
    } else {
        p.z = rr*.5/sqrt(xx+yy);
    }

//    std::cout << p.x << ", " << p.y << ", " << p.z << std::endl;

    return p;
}
