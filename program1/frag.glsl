#version 330

in vec4 fColor;
out vec4 color_out;

void main() {
  color_out = vec4(fColor.x, fColor.y, fColor.z, 1.0);
}
