#version 330

in vec2 position;
in vec3 vColor;
out vec4 fColor;
uniform mat4 ortho;

void main() {
  vec4 myVec = vec4(position.x, position.y, 0, 1);
  gl_Position = ortho * myVec;
  fColor = vec4(vColor.x, vColor.y, vColor.z, 1.0);
}
