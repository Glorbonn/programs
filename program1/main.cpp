#include <QApplication>
#include <iostream>
#include <string>
#include "glwidget.h"
using namespace std;

int main(int argc, char** argv) {

	
    QApplication a(argc, argv);

    QSurfaceFormat format;
    format.setVersion(3,3);
    format.setProfile(QSurfaceFormat::CoreProfile);
    QSurfaceFormat::setDefaultFormat(format);

    GLWidget glWidget;
	//cout <<"Please enter an image filename: " << endl;
	string filename;
	filename = string(argv[1]);
	const char *char_file_name = filename.c_str();
	glWidget.inputImage.load(char_file_name);
    qreal pixelRatio = glWidget.devicePixelRatio();
    glWidget.resize(glWidget.inputImage.width()/pixelRatio,glWidget.inputImage.height()/pixelRatio);
    glWidget.show();

    return a.exec();
}
