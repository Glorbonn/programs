#include "glwidget.h"
#include <iostream>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <QTextStream>
#include <vector>

using namespace std;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent), brushsize(25), incrementersize(1), size(true), drawRect(true), drawCirc(false), incremtoggle(false), num_pts(0), draw_points(0), drawMode(0) {
	myToolBar = new QToolBar();
	clicks[0].x = 0;
	clicks[0].y = 0;
	clicks[1].x = 0;
	clicks[1].y = 0;
    drawMode = GL_TRIANGLES;
}

GLWidget::~GLWidget() {
}

void GLWidget::keyPressEvent(QKeyEvent *event) {
    switch(event->key()) {
        case Qt::Key_C:
			cout << "Changed brush to circle mode." << endl;
			drawCirc = true;
			drawRect = false;
			incrementersize = 1;
            break;
        case Qt::Key_R:
			cout << "Changed brush to rectangle mode." << endl;
			drawRect = true;
			drawCirc = false;
            break;
        case Qt::Key_B:
			size = !size;
			if (size) {
				brushsize = 10;
			} else {
				brushsize = 5;
			}
            cout << "Brush size set to " << brushsize << "." << endl;
            break;
		case Qt::Key_F:
			cout << "Filling image with selected brush. " << endl;
			fill();
			break;
		case Qt::Key_I:
			incremtoggle = !incremtoggle;
			
			if (drawCirc == false) {
				drawCirc = true;
				drawRect = false;
			}
			if (incremtoggle) {
				incrementersize = 2;
				cout << "Toggling asterisk mode on. Circle mode on." << endl;
			} else {
				incrementersize = 1;
				cout << "Toggling asterisk mode off. Circle mode on." << endl;
			}
			break;
    }
	update();
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
	int x1 = 0;
	int y1 = 0;
	x1 = event->x();
	y1 = event->y();
	
	clicks[0].x = x1;
	clicks[0].y = y1;
	
	if (drawCirc == true) {
		int r = brushsize;
		drawCircles(r, clicks[0].x, clicks[0].y, incrementersize);
		glBindBuffer(GL_ARRAY_BUFFER, ptslistBuffer);
		glBufferData(GL_ARRAY_BUFFER, ptslist.size()*sizeof(ptslist[0]), &ptslist[0], GL_DYNAMIC_DRAW);	
		glBindBuffer(GL_ARRAY_BUFFER, clrslistBuffer);
		glBufferData(GL_ARRAY_BUFFER, clrslist.size()*sizeof(clrslist[0]), &clrslist[0], GL_DYNAMIC_DRAW);	
	}
}


/*
Draw rectangle of any size on release! Click once, drag, and release to draw a rectangle.
*/
void GLWidget::mouseReleaseEvent(QMouseEvent *event) {
	int x2 = 0;
	int y2 = 0;
	x2 = event->x();
	y2 = event->y();
	
	clicks[1].x = x2;
	clicks[1].y = y2;
	
	cout << "Added point (" << x2<< ", " << y2 << ") " << endl;
	
	if (drawRect == true) {
		drawRectangles(clicks[0].x, clicks[0].y, clicks[1].x, clicks[1].y);
		glBindBuffer(GL_ARRAY_BUFFER, ptslistBuffer);
		glBufferData(GL_ARRAY_BUFFER, ptslist.size()*sizeof(ptslist[0]), &ptslist[0], GL_DYNAMIC_DRAW);	
		glBindBuffer(GL_ARRAY_BUFFER, clrslistBuffer);
		glBufferData(GL_ARRAY_BUFFER, clrslist.size()*sizeof(clrslist[0]), &clrslist[0], GL_DYNAMIC_DRAW);	
	} 
}

void GLWidget::drawCircles(int r, int x, int y, int incrementer) {
	int maxpts = 24;
	draw_points = 24*3;
	ptslist.resize(num_pts+draw_points);
	for (int i = 0; i < maxpts; i+=incrementer) {
		ptslist[num_pts].x = x;
		ptslist[num_pts].y = y;
		ptslist[num_pts+1].x = x + (r*cos((2.0f*M_PI*(i+1))/maxpts));
		ptslist[num_pts+1].y = y + (r*sin((2.0f*M_PI*(i+1))/maxpts));
		ptslist[num_pts+2].x = x + (r*cos((2.0f*M_PI*(i+2))/maxpts));
		ptslist[num_pts+2].y=  y + (r*sin((2.0f*M_PI*(i+2))/maxpts));
		colorify();
		num_pts += 3;
	}
	update();
}

/*
Contains the logic for drawing rectangles.
*/

void GLWidget::drawRectangles(int x1, int y1, int x2, int y2) {
	/* Make first tri for rectangle in three steps */
	draw_points = 6;
	ptslist.resize(num_pts+6);
	for (int i = 0; i < 6; i++ ) {
		ptslist[num_pts+i].x = 0;
		ptslist[num_pts+i].y = 0;
	}
	
	ptslist[num_pts].x = x1; //1. define the user's click
	ptslist[num_pts].y = y1;	
	ptslist[num_pts+1].x = x2; //2. define the first corner of the first tri
	ptslist[num_pts+1].y = y1;
	ptslist[num_pts+2].x = x2; //3. these two points close the tri on the points given by the second user input point
	ptslist[num_pts+2].y = y2;
	//make second tri for rectangle	
	ptslist[num_pts+3].x = x2; //define the user's second click
	ptslist[num_pts+3].y = y2;
	ptslist[num_pts+4].x = x1; //create the first corner of the second triangle
	ptslist[num_pts+4].y = y2;
	ptslist[num_pts+5].x = x1; //closes the second triangle where the user's first point was entered
	ptslist[num_pts+5].y = y1;
	colorify(); //set up color values for each vertex based on the input image
	num_pts+=6;

	update();
}

/*
colorify takes a point from pts at a given index and uploads color information from input image
*/

void GLWidget::colorify() {
	clrslist.resize(num_pts+draw_points);
	for (int i = 0; i < draw_points; i++ ) {
		clrslist[num_pts].x = 0;
		clrslist[num_pts].y = 0;
		clrslist[num_pts].z = 0;
	}
	for (int i = 0; i < draw_points; i++ ) {
		if (ptslist[num_pts].x == inputImage.width()) {
			ptslist[num_pts].x = ptslist[num_pts].x - 1;
		}
		
		if (ptslist[num_pts].y == inputImage.height()) {
			ptslist[num_pts].y = ptslist[num_pts].y - 1;
		}
		QColor pixel_rgb = inputImage.pixel(ptslist[num_pts].x, ptslist[num_pts].y);
		unsigned int red = pixel_rgb.red();
		unsigned int blue = pixel_rgb.blue();
		unsigned int green = pixel_rgb.green();
		
		float norm_red = (float) red / 255.0f;
		float norm_blue = (float) blue / 255.0f;
		float norm_green = (float) green / 255.0f;
		clrslist[num_pts+i].x = norm_red;
		clrslist[num_pts+i].y = norm_green;
		clrslist[num_pts+i].z = norm_blue;
	}
}	


void GLWidget::paintGL() {

    glUseProgram(program);
    glBindVertexArray(vao);
	glDrawArrays(drawMode, 0, ptslist.size());

    // draw primitives based on the current draw mode
    //glDrawArrays(drawMode, 0, 6);
    
    // draw points so we can always see them
    // glPointSize adjusts the size of point
    // primitives
}



void GLWidget::fill() {
	int x1 = 0;
	int counter = 0;
	int x2 = 0;
	int y1 = 0;
	int y2 = 0;
	int numiter = 20000;
	
	if (brushsize) {
		numiter = 20000;
	} else {
		numiter = 40000;
	}
	while (counter < numiter) {
		int width = inputImage.width();
		int height = inputImage.height();
		get_rand(width);
		get_rand(height);
		
		x1 = 0;
		x2 = 0;
		y1 = 0;
		y2 = 0;
		
		x1 = get_rand(width);
		y1 = get_rand(height);
		if (counter % 2 == 0) {	
			x2 = x1 + get_rand(brushsize) + get_rand(brushsize);	
			y2 = y1 + get_rand(brushsize) + get_rand(brushsize);
		} else {
			x2 = x1 - get_rand(brushsize) - get_rand(brushsize);	
			y2 = y1 - get_rand(brushsize) - get_rand(brushsize);
		}

		
		
		if (drawRect == false && drawCirc == true) {
			x1 = get_rand(width-brushsize);
			y1 = get_rand(height-brushsize);
			drawCircles(brushsize + get_rand(brushsize), x1, y1, incrementersize);
		} else if (drawRect == true && drawCirc == false) {
			drawRectangles(x1, y1, x2, y2);	
		}
		colorify();
		counter += 1;
	}

	glBindBuffer(GL_ARRAY_BUFFER, ptslistBuffer);
	glBufferData(GL_ARRAY_BUFFER, ptslist.size()*sizeof(ptslist[0]), &ptslist[0], GL_DYNAMIC_DRAW);	
	glBindBuffer(GL_ARRAY_BUFFER, clrslistBuffer);
	glBufferData(GL_ARRAY_BUFFER, clrslist.size()*sizeof(clrslist[0]), &clrslist[0], GL_DYNAMIC_DRAW);	
}

int GLWidget::get_rand(int limit) {
    int divisor = RAND_MAX/(limit);
    int retval = 0;

    do { 
        retval = rand() / divisor;
    } while (retval > limit);

    return retval;
}
		

void GLWidget::initializeGL() {
    initializeOpenGLFunctions();
	
	glViewport(0,0,inputImage.width(),inputImage.height());

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glPointSize(4.0f);

    // Create a new Vertex Array Object on the GPU which
    // saves the attribute layout of our vertices.
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    program = loadShaders(":/vert.glsl", ":/frag.glsl");

    orthoMatrixLocation = glGetUniformLocation(program, "ortho");

	glGenBuffers(1, &ptslistBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, ptslistBuffer);
	glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);
	GLint ptslistIndex = glGetAttribLocation(program, "position");
	glEnableVertexAttribArray(ptslistIndex);
	glVertexAttribPointer(ptslistIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);

	
	glGenBuffers(1, &clrslistBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, clrslistBuffer);
	glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);
	GLint clrslistIndex = glGetAttribLocation(program, "vColor");
	glEnableVertexAttribArray(clrslistIndex);
	glVertexAttribPointer(clrslistIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);
	
	glClear(GL_COLOR_BUFFER_BIT);
}

void GLWidget::resizeGL(int w, int h) {
	float aspect = inputImage.width() / inputImage.height();
    glViewport(0,0,w,h);
	glm::mat4 orthomat(2.0f/(w),0.0f,0.0f,0.0f,
			      0.0f,2.0f/(-h/aspect),0.0f,0.0f,
				  0.0f,0.0f,-1.0f,0.0f,
			      -1.0f,1.0f,0.0f,1.0f);

	//glm::mat4 orthomat = glm::ortho(0.0f, (float) w, (float) h, 0.0f);
	glUseProgram(program);
	glUniformMatrix4fv(orthoMatrixLocation, 1, GL_FALSE, glm::value_ptr(orthomat));				


    // When our OpenGL context is resized, we need to change
    // our projection matrix to match. Create an orthographic 
    // projection matrix that converts window coordinates to normalized 
    // device coordinates.  This is similar to our previous lab, 
    // except the conversion will happen in our vertex shader. 
    // This way we won't need to do any conversions on our mouse 
    // event coordinates and when we resize the window the geometry 
    // won't be scaled unevenly.
}

// Copied from LoadShaders.cpp in the the oglpg-8th-edition.zip
// file provided by the OpenGL Programming Guide, 8th edition.
const GLchar* GLWidget::readShader(const char* filename) {
#ifdef WIN32
        FILE* infile;
        fopen_s( &infile, filename, "rb" );
#else
    FILE* infile = fopen( filename, "rb" );
#endif // WIN32

    if ( !infile ) {
#ifdef _DEBUG
        std::cerr << "Unable to open file '" << filename << "'" << std::endl;
#endif /* DEBUG */
        return NULL;
    }

    fseek( infile, 0, SEEK_END );
    int len = ftell( infile );
    fseek( infile, 0, SEEK_SET );

    GLchar* source = new GLchar[len+1];

    fread( source, 1, len, infile );
    fclose( infile );

    source[len] = 0;

    return const_cast<const GLchar*>(source);
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
    GLuint program = glCreateProgram();

    // read vertex shader from Qt resource file
    QFile vertFile(vertf);
    vertFile.open(QFile::ReadOnly | QFile::Text);
    QString vertString;
    QTextStream vertStream(&vertFile);
    vertString.append(vertStream.readAll());
    std::string vertSTLString = vertString.toStdString();

    const GLchar* vertSource = vertSTLString.c_str();

    GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertShader, 1, &vertSource, NULL);
    glCompileShader(vertShader);
    {
        GLint compiled;
        glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( vertShader, len, &len, log );
            std::cout << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, vertShader);

    // read fragment shader from Qt resource file
    QFile fragFile(fragf);
    fragFile.open(QFile::ReadOnly | QFile::Text);
    QString fragString;
    QTextStream fragStream(&fragFile);
    fragString.append(fragStream.readAll());
    std::string fragSTLString = fragString.toStdString();

    const GLchar* fragSource = fragSTLString.c_str();

    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, &fragSource, NULL);
    glCompileShader(fragShader);
    {
        GLint compiled;
        glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( fragShader, len, &len, log );
            std::cerr << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, fragShader);

    glLinkProgram(program);
    {
        GLint linked;
        glGetProgramiv( program, GL_LINK_STATUS, &linked );
        if ( !linked ) {
            GLsizei len;
            glGetProgramiv( program, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetProgramInfoLog( program, len, &len, log );
            std::cout << "Shader linker failed: " << log << std::endl;
            delete [] log;
        }
    }

    return program;
}




