#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__

#include <QOpenGLWidget>
#include <QToolbar>
#include <QOpenGLFunctions_3_3_Core>
#include <QMouseEvent>
#include <glm/glm.hpp>
#include <vector>
#include <math.h>

// glm by default uses degrees, but that functionality
// is deprecated so GLM_FORCE_RADIANS turns off some 
// glm warnings
#define GLM_FORCE_RADIANS

using glm::vec2;
using glm::vec3;

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core {
    Q_OBJECT

    public:
		QToolBar* myToolBar;
        GLWidget(QWidget *parent=0);
        ~GLWidget();
		QImage inputImage;

    protected:
        void initializeGL();
        void resizeGL(int w, int h);
        void paintGL();
		void fill();
		void colorify();
		int get_rand(int in);

        void mousePressEvent(QMouseEvent *event);
		void mouseReleaseEvent(QMouseEvent *event);
        void keyPressEvent(QKeyEvent *event);
		void drawRectangles(int x1, int y1, int x2, int y2);
		void drawCircles(int r, int x, int y, int incrementer);
		
		

    private:
        GLuint loadShaders(const char* vertf, const char* fragf);
        static const GLchar* readShader(const char* filename);

        GLuint vao;
        GLuint program;
		GLuint ptslistBuffer;
		GLuint clrslistBuffer;

        GLuint positionBuffer;
		GLuint colorBuffer;
		int brushsize;
		int incrementersize;
		bool size; //toggle between large/ small
		bool drawRect;
		bool drawCirc;
		bool incremtoggle;
		int num_pts; 
		int draw_points;
        GLenum drawMode;
		vec2 clicks[2];

        GLuint orthoMatrixLocation;
		
		std::vector<vec3> clrslist;
		std::vector<vec2> ptslist;

};

#endif
