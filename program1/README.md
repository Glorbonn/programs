# File Input
Program selects the file name from the command line. Example usage: program1.exe magpie

# Usage
The screen is initially blank. Press "f" to fill the image, or begin drawing. Multiple keypresses may be required to fill the image.

# Drawing
In rectangle mode, click, drag, and release to draw a rectangle. In circle mode, click once.

# Hotkeys

f: fill with current brush and brush size

c: switch to circle

r: switch to rectangle mode

b: switch between large and small brush sizes

i: switch to asterisk mode (side effect: switches to circle mode)


# Magpies

When I was growing up, I learned to appreciate all birds, even the city pigeons. 
Eventually I moved to Bozeman, and I began noticing the black-billed magie for the first time (pictured). 
I love to hear them make all the various squacks and calls that they do.
My family has even started to call out "Magpies!" whenever we see one. And then, last semester,
one came and latched onto my dorm window's screen! I chose to use the magpie for my image because I really enjoy them.

#Example Images
##Circles
![Circle](example_output_image_1_circs.PNG)
##Rectangles
![Rectangles](example_output_image_2_rects.PNG)
##Asterisks
![Asterisks](example_output_image_2_asterisks.PNG)
