#version 330

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

in vec3 position;
in vec3 color;
in vec3 normal;
in vec2 uv;

out vec3 fcolor;
out vec2 fuv;
out vec3 fpos;
out vec3 norm;

void main() {
  gl_Position = projection * view * model * vec4(position, 1);
  fcolor = color;
  //fuv = uv;
  norm = (transpose(inverse(model))*vec4(normal,0)).xyz;
  fpos = (model*vec4(position,1)).xyz;
}
