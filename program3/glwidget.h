#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__

#include <QGLWidget>
#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QMouseEvent>
#include <QTimer>
#include <glm/glm.hpp>

#define GLM_FORCE_RADIANS

using glm::mat3;
using glm::mat4;
using glm::vec3;

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core { 
    Q_OBJECT

	
    public:
        GLWidget(QWidget *parent=0);
        ~GLWidget();
			

        GLuint loadShaders(const char* vertf, const char* fragf);
    protected:
        void initializeGL();
        void resizeGL(int w, int h);
        void paintGL();

        void mousePressEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);
        void keyPressEvent(QKeyEvent *event);
        void keyReleaseEvent(QKeyEvent *event);
		
    public slots:

		// Animation slots for the first person camera (animate) and the cube animation (animateCube)
	
		void animate();
		void animateCube();

    private:
		QTimer *timer;

        float time;
	
		// Two methods for either drawing a single wall, or a 4 sided wall
		void drawWall(vec3 scale, vec3 spacing, vec3 location, int rows, int columns);
		void draw4Sided(vec3 scale, vec3 spacing, vec3 location, int rows, int columns);
		
        void initializeCube();
		
		//different rendering methods for different materials
        void renderCube(mat4 input);
		void renderCube2(mat4 input);
		void renderCube3(mat4 input);

        GLuint cubeProg;
        GLuint cubeVao;
        GLint cubeProjMatrixLoc;
        GLint cubeViewMatrixLoc;
        GLint cubeModelMatrixLoc;
        GLuint textureObject;

        void initializeGrid();
        void renderGrid();

        GLuint gridProg;
        GLuint gridVao;
        GLint gridProjMatrixLoc;
        GLint gridViewMatrixLoc;
        GLint gridModelMatrixLoc;
		
		// locations for phong shading
		GLint modelDiffuseColorLoc;
		GLint modelAmbientColorLoc;
		GLint modelLightPosLoc;
		GLint modelLightColorLoc;
		GLint modelLightIntensityLoc;

		// Fundamental matrices
        mat4 projMatrix;
        mat4 viewMatrix;
        mat4 modelMatrix;
		
		// animation sequence
        std::vector<vec3> rotations;

        // Camera vars for first person camera
		float yaw_angle;
		float pitch_angle;
		mat4 yaw;
		mat4 pitch;
		mat4 combined;
		mat4 camera;

		// Vars for determining key presses
		bool w, s, a, d, flying, shift, space, debug;
	
		vec3 position;
		vec3 velocity;

		// Vars for resizing
        int width;
        int height;

		// Vars for animation
        glm::vec2 lastPt;
		glm::vec3 lastVPt;
        void updateView();
		
		mat4 quatSlerp;
		
		// Unused var for global coloring
		vec3 color;		
};

#endif
