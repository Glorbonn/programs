# Hotkeys

* wasd - typical wasd movement
* tab - toggle flying (recommended!)

# Tips

Fly up and through some of the structures for cool visuals.

# The Haphazard World

Fly through the multicolored geometry with some spinners and walls for company.

# Lighting

4 sided walls have one lighting model. The animated cubes have the other two.

Phong shaded lighting on geometry. A few different light locations, and characteristics among the geometry. Mostly bright, but some
darkly shaded spinners in the world.

# Issues

* 3 different materials with different colorings, but hard to tell the difference.
* Colorings are defined differently, but do not appear different.
* Lags at the start

# Screenshots

![World2](example2.PNG)

![World3](example3.PNG)

![World](example.PNG)



