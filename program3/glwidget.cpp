#include "glwidget.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <iostream>
#include <QOpenGLTexture>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <QTextStream>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

using glm::inverse;
using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat3;
using glm::mat4;
using glm::perspective;
using glm::normalize;
using glm::length;
using glm::cross;
using glm::dot;
using glm::rotate;
using glm::translate;
using glm::value_ptr;
using glm::lookAt;
using glm::clamp;
using glm::quat;


using namespace std;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent) { 
	timer = new QTimer();
	connect(timer, SIGNAL(timeout()), this, SLOT(animate()));
	connect(timer, SIGNAL(timeout()), this, SLOT(animateCube()));
	timer->start(16);
	flying = false;
	w = false;
	s = false;
	a = false;
	d = false;
	shift = false;
	space = false;
	debug = true;
	flying = false;
	
	pitch_angle = 0.0f;
	yaw_angle = 0.0f;
	
	yaw = mat4(1.0f);
	pitch = mat4(1.0f);
	combined = mat4(1.0f);
	camera = mat4(1.0f);
	
	velocity = vec3(0.0f,0.0f,0.0f);
	position = vec3(0.2f,0.3f,0.0f);
	
	//scaling = vec3(0.12f,0.14f,0.15f);
	//color = vec3(1.0f,1.0f,1.0f);
	//spacing = vec3(.4f,.4f,.4f);
	
	//rows = 8;
	//columns = 8;
	
	timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(animate()));
    timer->start(16);

    quatSlerp = mat4(1.0f);

    time = 0;

    // rotations is an array of key frame euler angle orientations
    // feel free to change or add more key frames to the rotations
    // array.
    rotations.push_back(vec3(0, 0, 0));
    rotations.push_back(vec3(M_PI/3, M_PI/2, -4*M_PI));
    rotations.push_back(vec3(M_PI/3, M_PI/2, -4*M_PI));
}

GLWidget::~GLWidget() {
}

void GLWidget::initializeGrid() {

    glGenVertexArrays(1, &gridVao);
    glBindVertexArray(gridVao);

    // Create a buffer on the GPU for position data
    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);

    vec3 pts[84];
    for(int i = -10; i <= 10; i++) {

        pts[2*(i+10)] = vec3(i, -.5f, 10);
        pts[2*(i+10)+1] = vec3(i, -.5f, -10);

        pts[2*(i+10)+42] = vec3(10,-.5f, i);
        pts[2*(i+10)+43] = vec3(-10,-.5f, i);
    }
	
	//scale pts larger for larger ground
	for (int i = 0; i < 84; i++ ) {
		pts[i] = pts[i]*vec3(10,10,10);
	}

    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/grid_vert.glsl", ":/grid_frag.glsl");
    glUseProgram(program);
    gridProg = program;

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information 
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    gridProjMatrixLoc = glGetUniformLocation(program, "projection");
    gridViewMatrixLoc = glGetUniformLocation(program, "view");
    gridModelMatrixLoc = glGetUniformLocation(program, "model");
	
	modelDiffuseColorLoc = glGetUniformLocation(program, "diffuseColor");
    modelAmbientColorLoc = glGetUniformLocation(program, "ambientColor");

    modelLightPosLoc = glGetUniformLocation(program, "lightPos");
    modelLightColorLoc = glGetUniformLocation(program, "lightColor");
    modelLightIntensityLoc = glGetUniformLocation(program, "lightIntensity");

    glUniform3f(modelLightPosLoc, -50,100,-50);
    glUniform3f(modelLightColorLoc, 1,1,1);
    glUniform1f(modelLightIntensityLoc, 1);

    glUniform3f(modelAmbientColorLoc, .2, .2, .18);
    glUniform3f(modelDiffuseColorLoc, .25, .8, 1);
}

void GLWidget::initializeCube() {
    // Create a new Vertex Array Object on the GPU which
    // saves the attribute layout of our vertices.
    glGenVertexArrays(1, &cubeVao);
    glBindVertexArray(cubeVao);

    // Create a buffer on the GPU for position data
    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);
	
	GLuint normalBuffer;
	glGenBuffers(1, &normalBuffer);

    GLuint colorBuffer;
    glGenBuffers(1, &colorBuffer);

    GLuint uvBuffer;
    glGenBuffers(1, &uvBuffer);

    GLuint indexBuffer;
    glGenBuffers(1, &indexBuffer);

    vec3 pts[] = {
        // top
        vec3(1,1,1),    // 0
        vec3(1,1,-1),   // 1
        vec3(-1,1,-1),  // 2
        vec3(-1,1,1),   // 3

        // bottom
        vec3(1,-1,1),   // 4
        vec3(-1,-1,1),  // 5
        vec3(-1,-1,-1), // 6
        vec3(1,-1,-1),  // 7

        // front
        vec3(1,1,1),    // 8
        vec3(-1,1,1),   // 9
        vec3(-1,-1,1),  // 10
        vec3(1,-1,1),   // 11

        // back
        vec3(-1,-1,-1), // 12
        vec3(-1,1,-1),  // 13
        vec3(1,1,-1),   // 14
        vec3(1,-1,-1),  // 15

        // right
        vec3(1,-1,1),   // 16
        vec3(1,-1,-1),  // 17
        vec3(1,1,-1),   // 18
        vec3(1,1,1),     // 19

        // left
        vec3(-1,-1,1),  // 20
        vec3(-1,1,1),   // 21
        vec3(-1,1,-1),  // 22
        vec3(-1,-1,-1) // 23

    };
	
		vec3 norms[] = {
		// top
        vec3(0,1,0),    // 0
        vec3(0,1,0),    // 1
        vec3(0,1,0),    // 2
        vec3(0,1,0),    // 3

        // bottom
        vec3(0,-1,0),   // 4
        vec3(0,-1,0),   // 5
        vec3(0,-1,0),   // 6
        vec3(0,-1,0),   // 7

        // front
        vec3(0,0,1),    // 8
        vec3(0,0,1),    // 9
        vec3(0,0,1),    // 10
        vec3(0,0,1),    // 11

        // back
        vec3(0,0,-1),   // 12
        vec3(0,0,-1),   // 13
        vec3(0,0,-1),   // 14
        vec3(0,0,-1),   // 15

        // right
        vec3(1,0,0),  	// 16
        vec3(1,0,0),    // 17
        vec3(1,0,0),  	// 18
        vec3(1,0,0),    // 19

        // left
        vec3(-1,0,0),   // 20
        vec3(-1,0,0),   // 21
        vec3(-1,0,0),   // 22
        vec3(-1,0,0)    // 23
		
	
	};

    for(int i = 0; i < 24; i++) {
        pts[i] *= .5;
    }

    vec3 colors[] = {
        // top
        vec3(0,1,0),    
        vec3(0,1,0),    
        vec3(0,1,0),    
        vec3(0,1,0),    

        // bottom
        vec3(0,.5f,0),  
        vec3(0,.5f,0),  
        vec3(0,.5f,0),  
        vec3(0,.5f,0),  

        // front
        vec3(0,0,1),    
        vec3(0,0,1),    
        vec3(0,0,1),    
        vec3(0,0,1),    

        // back
        vec3(0,0,.5f),  
        vec3(0,0,.5f),  
        vec3(0,0,.5f),  
        vec3(0,0,.5f),

        // right
        vec3(1,0,0),    
        vec3(1,0,0),    
        vec3(1,0,0),    
        vec3(1,0,0),    


        // left
        vec3(.5f,0,0),  
        vec3(.5f,0,0),  
        vec3(.5f,0,0),  
        vec3(.5f,0,0)  
    };

    vec2 uvs[] = {
        // top
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),

        // bottom
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),

        // front
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),

        // back
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),

        // right
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),

        // left
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0)

    };

    GLuint restart = 0xFFFFFFFF;
    GLuint indices[] = {
        0,1,2,3, restart,
        4,5,6,7, restart,
        8,9,10,11, restart,
        12,13,14,15, restart,
        16,17,18,19, restart,
        20,21,22,23
    };

    // Upload the position data to the GPU, storing
    // it in the buffer we just allocated.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(uvs), uvs, GL_STATIC_DRAW);
	
	glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(norms), norms, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/vert.glsl", ":/frag.glsl");
    glUseProgram(program);
    cubeProg = program;

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information 
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    GLint colorIndex = glGetAttribLocation(program, "color");
    glEnableVertexAttribArray(colorIndex);
    glVertexAttribPointer(colorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
    GLint uvIndex = glGetAttribLocation(program, "uv");
    glEnableVertexAttribArray(uvIndex);
    glVertexAttribPointer(uvIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);
	
	glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    GLint normalIndex = glGetAttribLocation(program, "normal");
    glEnableVertexAttribArray(normalIndex);
    glVertexAttribPointer(normalIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    // The following is an 8x8 checkerboard pattern using
    // GL_RED, GL_UNSIGNED_BYTE data.
    static const GLubyte tex_checkerboard_data[] =
    {
        0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00,
        0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF,
        0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00,
        0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF,
        0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00,
        0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF,
        0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00,
        0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF
    };

    glGenTextures(1, &textureObject);
    glBindTexture(GL_TEXTURE_2D, textureObject);

//    QImage img = QImage("orange.jpg").toImageFormat(QImage::Format_ARGB32);
//    QImage img("orange.jpg");
//    std::cout << img.format() << std::endl;
//    QOpenGLTexture tex(img);
//    std::cout << tex.format() << std::endl;

    glTexImage2D(GL_TEXTURE_2D, 0,GL_RED,8,8,0,GL_RED,GL_UNSIGNED_BYTE,tex_checkerboard_data);

//    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA,img.width(),img.height(),0,GL_RGBA,GL_UNSIGNED_INT_8_8_8_8,img.bits());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    cubeProjMatrixLoc = glGetUniformLocation(program, "projection");
    cubeViewMatrixLoc = glGetUniformLocation(program, "view");
    cubeModelMatrixLoc = glGetUniformLocation(program, "model");

//    GLint texLoc = glGetUniformLocation(program, "tex");
//    glUniform1i(texLoc, 0);

    modelDiffuseColorLoc = glGetUniformLocation(program, "diffuseColor");
    modelAmbientColorLoc = glGetUniformLocation(program, "ambientColor");

    modelLightPosLoc = glGetUniformLocation(program, "lightPos");
    modelLightColorLoc = glGetUniformLocation(program, "lightColor");
    modelLightIntensityLoc = glGetUniformLocation(program, "lightIntensity");

    glUniform3f(modelLightPosLoc, -50,100,-50);
    glUniform3f(modelLightColorLoc, 1,1,1);
    glUniform1f(modelLightIntensityLoc, 1);

    glUniform3f(modelAmbientColorLoc, .2, .2, .18);
    glUniform3f(modelDiffuseColorLoc, 1, 1, 1);
}

//Draw a single wall! Positions are changed using translate and iterating using rows and columns.
void GLWidget::drawWall(vec3 scale, vec3 spacing, vec3 location, int rows, int columns) {
	vec3 iterateSpacing(spacing.x, spacing.y, spacing.z); //unnecessary

	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < columns; j++) {
			mat4 cube = glm::scale(mat4(1.0f), vec3(scale.x, scale.y, scale.z));
			renderCube(translate(cube, vec3(iterateSpacing.x*j+location.x,iterateSpacing.y*i+location.y,iterateSpacing.z+location.z)));
			//iterateSpacing.z += iterateSpacing.z;
			//spacing.z += spacing.z;
		}
		//std::cout << iterateSpacing.x << " " << iterateSpacing.y << " " << iterateSpacing.z << std::endl;
		//iterateSpacing.y += iterateSpacing.y;
	}

	//spacing.z 
}

void GLWidget::draw4Sided(vec3 scaling, vec3 spacing, vec3 location, int rows, int columns) {
	vec3 iterateSpacing(spacing.x, spacing.y, spacing.z);

		//draw front wall
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				mat4 cube = glm::scale(mat4(1.0f), vec3(scaling.x, scaling.y, scaling.z));
				renderCube(translate(cube, vec3(iterateSpacing.x*j+location.x,iterateSpacing.y*i+location.y,iterateSpacing.z+location.z)));
				//iterateSpacing.z += iterateSpacing.z;
				//spacing.z += spacing.z;
			}
			//std::cout << iterateSpacing.x << " " << iterateSpacing.y << " " << iterateSpacing.z << std::endl;
			//iterateSpacing.y += iterateSpacing.y;
		}
		
		//draw left and right walls
		for (int i = 0; i < rows; i++) { 
			for (int j = 0; j < columns; (j+=columns-1)) { //skip all but the first and last walls!
				for (int k = 1; k < columns; k++ ) { //make cubes in the right and left walls
				mat4 cube = glm::scale(mat4(1.0f), vec3(scaling.x, scaling.y, scaling.z));
				renderCube(translate(cube, vec3(iterateSpacing.x*j+location.x,iterateSpacing.y*i+location.y,iterateSpacing.z*k+location.z)));
				//iterateSpacing.z += iterateSpacing.z;
				//spacing.z += spacing.z;
				}
			}
			//std::cout << iterateSpacing.x << " " << iterateSpacing.y << " " << iterateSpacing.z << std::endl;
			//iterateSpacing.y += iterateSpacing.y;
		}
		
		//draw back wall
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				mat4 cube = glm::scale(mat4(1.0f), vec3(scaling.x, scaling.y, scaling.z));
				renderCube(translate(cube, vec3(iterateSpacing.x*j+location.x,iterateSpacing.y*i+location.y,iterateSpacing.z*columns+location.z)));
				//iterateSpacing.z += iterateSpacing.z;
				//spacing.z += spacing.z;
			}
			//std::cout << iterateSpacing.x << " " << iterateSpacing.y << " " << iterateSpacing.z << std::endl;
			//iterateSpacing.y += iterateSpacing.y;
		}
	


	//spacing.z 
}

void GLWidget::initializeGL() {
    initializeOpenGLFunctions();

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glPointSize(4.0f);

    glEnable(GL_DEPTH_TEST);
    GLuint restart = 0xFFFFFFFF;
    glPrimitiveRestartIndex(restart);
    glEnable(GL_PRIMITIVE_RESTART);

    initializeCube();
    initializeGrid();

    viewMatrix = mat4(1.0f);
    modelMatrix = mat4(1.0f);

    glUniformMatrix4fv(cubeViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUseProgram(cubeProg);
    glUniformMatrix4fv(cubeModelMatrixLoc, 1, false, value_ptr(modelMatrix));

    glUseProgram(gridProg);
    glUniformMatrix4fv(gridViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(gridModelMatrixLoc, 1, false, value_ptr(modelMatrix));
}

void GLWidget::resizeGL(int w, int h) {
    width = w;
    height = h;

    float aspect = (float)w/h;

    projMatrix = perspective(70.0f, aspect, .01f, 1000.0f);

    glUseProgram(cubeProg);
    glUniformMatrix4fv(cubeProjMatrixLoc, 1, false, value_ptr(projMatrix));

    glUseProgram(gridProg);
    glUniformMatrix4fv(gridProjMatrixLoc, 1, false, value_ptr(projMatrix));
}

void GLWidget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    renderGrid();
    //renderCube(mat4(1.0f));
	mat4 animCube1 = glm::scale(mat4(1.0f), vec3(2,2,2));
	animCube1 = glm::translate(animCube1, vec3(-.8,0,0))*quatSlerp;
	renderCube3(animCube1);
	//renderCube(glm::translate(mat4(1.0f), vec3(-.8,0,0))*quatSlerp);
	
	//scatter some animated cubes around
	mat4 animCube2 = glm::scale(mat4(1.0f), vec3(2,2,2));
	animCube2 = glm::translate(animCube2, vec3(-.8,4,0))*quatSlerp;
	renderCube3(animCube2);
	
	mat4 animCube3 = glm::scale(mat4(1.0f), vec3(1,1,9));
	animCube3 = glm::translate(animCube3, vec3(-.8,8,0))*quatSlerp;
	renderCube3(animCube3);
	
	mat4 animCube4 = glm::scale(mat4(1.0f), vec3(1,1,2));
	animCube4 = glm::translate(animCube4, vec3(5,9,10))*quatSlerp;
	renderCube3(animCube4);
	
	mat4 animCube5 = glm::scale(mat4(1.0f), vec3(2,2,2));
	animCube5 = glm::translate(animCube5, vec3(-.8,0,0))*quatSlerp;
	renderCube3(animCube5);
	
	mat4 animCube6 = glm::scale(mat4(1.0f), vec3(4,4,4));
	animCube6 = glm::translate(animCube6, vec3(-.8,0,16))*quatSlerp;
	renderCube2(animCube6);
	
	mat4 animCube7 = glm::scale(mat4(1.0f), vec3(4,4,4));
	animCube7 = glm::translate(animCube7, vec3(27,0,27))*quatSlerp;
	renderCube2(animCube7);
	
	mat4 animCube8 = glm::scale(mat4(1.0f), vec3(4,4,25));
	animCube8 = glm::translate(animCube8, vec3(29,0,40))*quatSlerp;
	renderCube2(animCube8);
	
	mat4 animCube9 = glm::scale(mat4(1.0f), vec3(4,4,4));
	animCube9 = glm::translate(animCube9, vec3(12,50,25))*quatSlerp;
	renderCube2(animCube9);
	
	mat4 animCube10 = glm::scale(mat4(1.0f), vec3(12,12,12));
	animCube10 = glm::translate(animCube10, vec3(50,50,50))*quatSlerp;
	renderCube2(animCube10);

	
	vec3 wall_1_spacing = vec3(1,1.3,1);
	vec3 wall_1_location = vec3(2,0,2);
	vec3 wall_1_scaling = vec3(.5,.5,.5);
	draw4Sided(wall_1_scaling, wall_1_spacing, wall_1_location, 8, 4);
	
	vec3 wall_2_spacing = vec3(1,2,1.2);
	vec3 wall_2_location = vec3(-100,0,100);
	vec3 wall_2_scaling = vec3(.1,2,.1);
	draw4Sided(wall_2_scaling, wall_2_spacing, wall_2_location, 24, 8);
	//draw4Sided(mat4(1.0f),mat4(1.0f),mat4(1.0f));
	
	vec3 wall_3_spacing = vec3(1,2,1.2);
	vec3 wall_3_location = vec3(100,0,100);
	vec3 wall_3_scaling = vec3(.1,2,.1);
	draw4Sided(wall_3_scaling, wall_3_spacing, wall_3_location, 24, 8);
	
	vec3 wall_4_spacing = vec3(1,2,1.2);
	vec3 wall_4_location = vec3(50,0,100);
	vec3 wall_4_scaling = vec3(.1,2,.1);
	draw4Sided(wall_4_scaling, wall_4_spacing, wall_4_location, 24, 24);
	
	vec3 wall_5_spacing = vec3(1,1,1);
	vec3 wall_5_location = vec3(25,0,100);
	vec3 wall_5_scaling = vec3(.3,2,.1);
	draw4Sided(wall_5_scaling, wall_5_spacing, wall_5_location, 24, 24);
	
	vec3 wall_6_spacing = vec3(1,1,1);
	vec3 wall_6_location = vec3(12,0,100);
	vec3 wall_6_scaling = vec3(1,1,1);
	draw4Sided(wall_6_scaling, wall_6_spacing, wall_6_location, 24, 24);
	
	vec3 wall_7_spacing = vec3(1,1,1);
	vec3 wall_7_location = vec3(-50,0,-50);
	vec3 wall_7_scaling = vec3(100,100,100);
	draw4Sided(wall_7_scaling, wall_7_spacing, wall_7_location, 24, 6);
	
	vec3 wall_8_spacing = vec3(1,12,1);
	vec3 wall_8_location = vec3(0,0,0);
	vec3 wall_8_scaling = vec3(4,1,4);
	drawWall(wall_8_scaling, wall_8_spacing, wall_8_location, 64, 64);
	
	vec3 wall_9_spacing = vec3(1,12,1);
	vec3 wall_9_location = vec3(100,0,0);
	vec3 wall_9_scaling = vec3(4,1,4);
	drawWall(wall_9_scaling, wall_9_spacing, wall_9_location, 2, 64);
	
	vec3 wall_10_spacing = vec3(1,12,1);
	vec3 wall_10_location = vec3(-50,0,0);
	vec3 wall_10_scaling = vec3(4,1,4);
	drawWall(wall_10_scaling, wall_10_spacing, wall_10_location, 4, 4);
}


// Render a different material!
void GLWidget::renderCube3(mat4 cubeToRender) {

    glUseProgram(cubeProg);
    glBindVertexArray(cubeVao);
	
	GLuint colorBuffer;
    glGenBuffers(1, &colorBuffer);
	
	vec3 colors[24];
	for (int i = 0; i < 24; i++ ) {
		colors[i] = vec3(1,.2,.9);
	}
	
	glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);	
	glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);
	
	modelDiffuseColorLoc = glGetUniformLocation(cubeProg, "diffuseColor");
    modelAmbientColorLoc = glGetUniformLocation(cubeProg, "ambientColor");

    modelLightPosLoc = glGetUniformLocation(cubeProg, "lightPos");
    modelLightColorLoc = glGetUniformLocation(cubeProg, "lightColor");
    modelLightIntensityLoc = glGetUniformLocation(cubeProg, "lightIntensity");

    glUniform3f(modelLightPosLoc, -50,100,-50);
    glUniform3f(modelLightColorLoc, 0,1,0);
    glUniform1f(modelLightIntensityLoc, 1.6);

    glUniform3f(modelAmbientColorLoc, 0, .1, .1);
    glUniform3f(modelDiffuseColorLoc, .5, 0, 0);
	//cubeToRender = scale(cubeToRender, scaling);
	modelMatrix = cubeToRender;

	glUniformMatrix4fv(cubeModelMatrixLoc, 1, false, value_ptr(modelMatrix));

    glBindTexture(GL_TEXTURE_2D, textureObject);

    glDrawElements(GL_TRIANGLE_FAN, 29, GL_UNSIGNED_INT, 0);
}
// Render a different material!
void GLWidget::renderCube2(mat4 cubeToRender) {	
    glUseProgram(cubeProg);
    glBindVertexArray(cubeVao);
	
	GLuint colorBuffer;
    glGenBuffers(1, &colorBuffer);
	
	vec3 colors[24];
	for (int i = 0; i < 24; i++ ) {
		colors[i] = vec3(0,.2,.9);
	}
	
	glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);	
	glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);

	modelDiffuseColorLoc = glGetUniformLocation(cubeProg, "diffuseColor");
    modelAmbientColorLoc = glGetUniformLocation(cubeProg, "ambientColor");

    modelLightPosLoc = glGetUniformLocation(cubeProg, "lightPos");
    modelLightColorLoc = glGetUniformLocation(cubeProg, "lightColor");
    modelLightIntensityLoc = glGetUniformLocation(cubeProg, "lightIntensity");

    glUniform3f(modelLightPosLoc, -50,100,-50);
    glUniform3f(modelLightColorLoc, 0,0,1);
    glUniform1f(modelLightIntensityLoc, 1);

    glUniform3f(modelAmbientColorLoc, .2, .2, .18);
    glUniform3f(modelDiffuseColorLoc, 0, 0, 1);
	//cubeToRender = scale(cubeToRender, scaling);
	modelMatrix = cubeToRender;

	glUniformMatrix4fv(cubeModelMatrixLoc, 1, false, value_ptr(modelMatrix));

    //glBindTexture(GL_TEXTURE_2D, textureObject);

    glDrawElements(GL_TRIANGLE_FAN, 29, GL_UNSIGNED_INT, 0);
}

// Render a different material!
void GLWidget::renderCube(mat4 cubeToRender) {
    glUseProgram(cubeProg);
    glBindVertexArray(cubeVao);
	//cubeToRender = scale(cubeToRender, scaling);
	modelMatrix = cubeToRender;
	
	modelDiffuseColorLoc = glGetUniformLocation(cubeProg, "diffuseColor");
    modelAmbientColorLoc = glGetUniformLocation(cubeProg, "ambientColor");

    modelLightPosLoc = glGetUniformLocation(cubeProg, "lightPos");
    modelLightColorLoc = glGetUniformLocation(cubeProg, "lightColor");
    modelLightIntensityLoc = glGetUniformLocation(cubeProg, "lightIntensity");

    glUniform3f(modelLightPosLoc, 50,0,50);
    glUniform3f(modelLightColorLoc, 1,1,1);
    glUniform1f(modelLightIntensityLoc, 1);

    glUniform3f(modelAmbientColorLoc, .8, .8, .8);
    glUniform3f(modelDiffuseColorLoc, 1, 1, 1);

	glUniformMatrix4fv(cubeModelMatrixLoc, 1, false, value_ptr(modelMatrix));

    //glBindTexture(GL_TEXTURE_2D, textureObject);

    glDrawElements(GL_TRIANGLE_FAN, 29, GL_UNSIGNED_INT, 0);
}

void GLWidget::renderGrid() {
    glUseProgram(gridProg);
    glBindVertexArray(gridVao);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 84);
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
    GLuint program = glCreateProgram();

    // read vertex shader from Qt resource file
    QFile vertFile(vertf);
    vertFile.open(QFile::ReadOnly | QFile::Text);
    QString vertString;
    QTextStream vertStream(&vertFile);
    vertString.append(vertStream.readAll());
    std::string vertSTLString = vertString.toStdString();

    const GLchar* vertSource = vertSTLString.c_str();

    GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertShader, 1, &vertSource, NULL);
    glCompileShader(vertShader);
    {
        GLint compiled;
        glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( vertShader, len, &len, log );
            std::cout << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, vertShader);

    // read fragment shader from Qt resource file
    QFile fragFile(fragf);
    fragFile.open(QFile::ReadOnly | QFile::Text);
    QString fragString;
    QTextStream fragStream(&fragFile);
    fragString.append(fragStream.readAll());
    std::string fragSTLString = fragString.toStdString();

    const GLchar* fragSource = fragSTLString.c_str();

    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, &fragSource, NULL);
    glCompileShader(fragShader);
    {
        GLint compiled;
        glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( fragShader, len, &len, log );
            std::cerr << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, fragShader);

    glLinkProgram(program);
    {
        GLint linked;
        glGetProgramiv( program, GL_LINK_STATUS, &linked );
        if ( !linked ) {
            GLsizei len;
            glGetProgramiv( program, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetProgramInfoLog( program, len, &len, log );
            std::cout << "Shader linker failed: " << log << std::endl;
            delete [] log;
        }
    }

    return program;
}

void GLWidget::keyPressEvent(QKeyEvent *event) {
    switch(event->key()) {	
        case Qt::Key_W:
			w = true;
            // forward
            break;
        case Qt::Key_A:     
			a = true;
			// left
            break;
        case Qt::Key_S:
			// back
			s = true;
            break;
        case Qt::Key_D:
			// right
			d = true;
            break;
        case Qt::Key_Tab:
            flying = !flying;
			//toggle flying
            break;
        case Qt::Key_Shift:
			shift = true;
            // down
            break;
        case Qt::Key_Space:
			space = true;
            // up or jump
            break;
    }
}

void GLWidget::keyReleaseEvent(QKeyEvent *event) {
    switch(event->key()) {
        case Qt::Key_W:
			w = false;
            // forward
            break;
        case Qt::Key_A:
			a = false;
            // left
            break;
        case Qt::Key_S:
			s = false;
            // back
            break;
        case Qt::Key_D:
			d = false;
            // right
            break;
        case Qt::Key_Tab:
            // toggle fly mode
            break;
        case Qt::Key_Shift:
			shift = false;
            // down
            break;
        case Qt::Key_Space:
			space = false;
            // up or jump
            break;
    }
}

void GLWidget::animateCube() {
    // increment time by our time step
    float dt = .016;
    time += dt;
	
	// we want 2 seconds of animation per rotation then to start over, so 
    // restart to 0 once we've reached our max time
    if(time > 2*(rotations.size()-1)) {
        time = 0;
    }
	
	// Convert time to a value between 0 and 1
    // at 0 we're at the beginning of our rotations
    // array, and at 1 we've reach the last one
    float t = fmin(time/(2*(rotations.size()-1)),1);

    // Get two indices into our rotations array
    // that represent our current animation
    unsigned int fromIndex = t*(rotations.size()-1);
    unsigned int toIndex = fromIndex+1;
	
	// when t = 1 toIndex will be out of range, so
    // just clamp it to the last index in the array
    if(toIndex > rotations.size()-1) {
        toIndex = rotations.size()-1;
    }

    // we want t to be a 0-1 value that represents the
    // percentage between two consecutive indices, so
    // get our current index as a floating point number
    // then subtract off the integer portion
    t = t*(rotations.size()-1)-(int)(t*(rotations.size()-1));
	
	// Euler angle representations of 
    vec3 from = rotations[fromIndex];
    vec3 to = rotations[toIndex];
	
	// Part 3 - Quaternions are another way to represent orientation. 
    // glm has a quaternion data structure called quat. It's constructor
    // can take a vec3 that represents Euler angles. Construct two quaternions
    // using the from and to euler angles.
	
	quat from_quat(from);
	quat to_quat(to);


    // Interpolate the two quaternions using glm::slerp. slerp stands for
    // spherical linear interpolation and is how quaternions can be animated
    // along the shortest path. glm::slerp takes 3 arguments:
    // glm::slerp(glm::quat q1, glm::quat q2, float t)
    // where t is in the range 0-1 and returns a quaternion t percent
    // between q1 and q2
	
	quat slerping = glm::slerp(from_quat, to_quat, t);

    // The last step is to convert the resulting quaternion into a matrix
    // for use in our fragment shader. Use glm::toMat4(glm::quat) to do so
    // and store the resulting matrix in quatSlerp. Again, quatSlerp is used
    // in paintGL to render our third cube.
	
	quatSlerp = glm::toMat4(slerping);

    update();

}

void GLWidget::animate() {
	update();
	
	/*
	Logic for key presses
	*/
	vec3 right(yaw[0][0],yaw[0][1], yaw[0][2]);
	vec3 forward(yaw[2][0], yaw[2][1], yaw[2][2]);
	vec3 down(0,1,0);
	vec3 fly(combined[2][0],combined[2][1],combined[2][2]);
	
	/*if (debug) {
		
		cout << " RIGHT " << endl;
		cout << right.x << " " << right.y << " " << right.z << endl;
		
		
	}*/
	
	
	
	if (!flying && w) {
		velocity += forward * -1.0f;
		//cout << length(velocity) << endl;
	} 
	
	if (flying && w) {
		velocity += fly * -1.0f;
	} 
	
	if(!flying && s) {
		velocity += forward;
	} 
	
	if(flying && s) {
		velocity += fly;
	}
	
	if(a) {
		velocity += right * -1.0f;
	}
	
	if(d) {
		velocity += right;
	}
	
	if(space) {
		velocity += down*3.0f;
	}
	
	//Gravity
	
	if (!flying) {
		if (position.y > 0.000001f) {
			velocity -= down;
		}
	}
	

	
	/*if(debug) {
		cout << " VELOCITY " << endl;
		cout << velocity.x << " " << velocity.y << " " << velocity.z << endl;
		
		cout << " POSITION " << endl;
		cout << position.x << " " << position.y << " " << position.z << endl;
		debug = false;
	}*/
	
	
	if (length(velocity) > 0.0000001f) {
		position += normalize(velocity)*0.0485f;
		//debug
		//cout << " POSITION " << endl;
		//cout << position.x << " " << position.y << " " << position.z << endl;
	}
	
	//Define program (state machine!)
	
	
	
	//Define camera
	
	camera = inverse(translate(mat4(1.0f), position)*yaw*pitch);
	
	// Upload camera to view matrix
	glUseProgram(cubeProg);
	glUniformMatrix4fv(cubeViewMatrixLoc, 1, false, value_ptr(camera));
	glUseProgram(gridProg);
	glUniformMatrix4fv(gridViewMatrixLoc, 1, false, value_ptr(camera));
	
	//Stop the camera between each integration step
	
	velocity.x = 0.0f;
	velocity.y = 0.0f;
	velocity.z = 0.0f;
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
    vec2 pt(event->x(), event->y());
    lastPt = pt;
}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
    vec2 pt(event->x(), event->y());
    vec2 d = pt-lastPt;
	//std::cout << d.x << std::endl;
	
	//int x_change = d.x - pt.x;
	//int y_change = d.y - pt.y;
	
	lastPt = pt;
	
    // Part 1 - use d.x and d.y to modify your pitch and yaw angles
    // before constructing pitch and yaw rotation matrices with them
	
		
	pitch_angle += d.y*.01;
	yaw_angle += d.x*.01;
	
	if (pitch_angle >= 90*M_PI/180.0f) {
		pitch_angle = 90*M_PI/180.0f;
	}
	
	if (pitch_angle <= -90*M_PI/180.f) {
		pitch_angle = -90*M_PI/180.f;
	}
	
	pitch = rotate(mat4(1.0f), pitch_angle, vec3(1, 0, 0));
	yaw = rotate(mat4(1.0f), yaw_angle, vec3(0,1,0));

	combined = yaw*pitch;
	
	//combined = inverse(yaw*pitch);
	//glUseProgram(cubeProg);
	//glUniformMatrix4fv(cubeViewMatrixLoc, 1, false, value_ptr(combined));

	
	
}
