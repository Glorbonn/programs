#version 330

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

in vec3 position;
out vec4 fcolor;

void main() {
  gl_Position = projection * view * model * vec4(position, 1);
  fcolor = projection*view*model*vec4(position,1);
}
