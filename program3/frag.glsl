#version 330

uniform vec3 lightPos;
uniform vec3 lightColor;
uniform float lightIntensity;
uniform sampler2D tex;

uniform vec3 ambientColor;
uniform vec3 diffuseColor;

uniform mat4 view;

in vec3 fcolor;
in vec3 norm;
in vec2 fuv;
in vec3 fpos;

out vec4 color_out;

void main() {
    vec3 L = normalize(lightPos-fpos);
    vec3 N = normalize(norm);
	vec3 E = normalize(fpos);
	vec3 R = normalize(-reflect(L,N));
	
	float angle = max(0.0, dot(E,R));
	float specCoeff = pow(angle, .012);
	
	vec3 diff = diffuseColor*max(dot(N,L),0.0);
	vec3 spec = specCoeff + vec3(1.0,1.0,1.0) * .012;
	spec = clamp(spec,0.0,1.0);
	
	color_out = vec4(fcolor,1)* vec4(ambientColor + diff + spec,1);

    //vec3 diffuse = diffuseColor*dot(N,L);
	//color_out = texture(tex,fuv).rgba*vec4(lightIntensity*lightColor*diffuse+ambientColor, 1);
	//  color_out = vec4(fuv,0,1);
}
