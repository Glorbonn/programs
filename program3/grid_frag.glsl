#version 330

uniform vec3 lightPos;
uniform vec3 lightColor;
uniform float lightIntensity;
uniform vec3 ambientColor;
uniform vec3 diffuseColor;

out vec4 color_out;
in vec4 fcolor;

void main() {
  color_out = vec4(.67,.67,.67, 1)*vec4(lightIntensity*lightColor+ambientColor, 1);
}
